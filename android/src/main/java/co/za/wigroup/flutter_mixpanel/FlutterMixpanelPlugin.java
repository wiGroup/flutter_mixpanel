package co.za.wigroup.flutter_mixpanel;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * Copyright © 2021 wigroup. All rights reserved.
 *
 * Created by Sooraj Beebakhy on 27/01/2021
 * 
 * Email: sooraj.beebakhy@yoyowallet.com
 * Website: www.wigroupinternational.com
 */

public class FlutterMixpanelPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
    private MethodChannel channel;
    private static Activity activity;
    private MixpanelAPI mixpanelAPI;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flutter_mixpanel");
        channel.setMethodCallHandler(this);
    }

    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_mixpanel");
        channel.setMethodCallHandler(new FlutterMixpanelPlugin());
        FlutterMixpanelPlugin.activity = registrar.activity();
    }

    private Map<String, Double> jsonToNumberMap(JSONObject jsonObj) throws JSONException {
        HashMap<String, Double> map = new HashMap<>();
        Iterator<String> keys = jsonObj.keys();

        while (keys.hasNext()) {
            String key = keys.next();
            map.put(key, jsonObj.getDouble(key));
        }

        return map;
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        if (call.method.equals("initialize")) {
            initMixPanel(call.arguments.toString(), result);
        } else if (mixpanelAPI != null) {
            switch ((call.method)) {
                case "identify":
                    identify(call.arguments.toString(), result);
                    break;
                case "getDistinctId":
                    getDistinctId(result);
                    break;
                case "identifyPeople":
                    identifyPeople(call.arguments.toString(), result);
                    break;
                case "alias":
                    alias(call.arguments.toString(), result);
                    break;
                case "setPeopleProperties":
                    setPeopleProperties(call, result);
                    break;
                case "setOnce":
                    setOnce(call, result);
                    break;
                case "incrementPeopleProperties":
                    incrementPeopleProperties(call, result);
                    break;
                case "registerSuperProperties":
                    registerSuperProperties(call, result);
                    break;
                case "reset":
                    reset(result);
                    break;
                case "flush":
                    flush(result);
                    break;
                case "showNotificationIfAvailable":
                    showNotification();
                    break;
                case "removeAllPushDeviceTokens":
                    clearPushRegistrationId();
                    break;
                case "addPushDeviceToken":
                    pushRegistrationId(call.arguments.toString(), result);
                    break;
                default:
                    trackEvent(call, result);
                    break;
            }
        }
    }
    
    private void getDistinctId(Result result) {
        String mixpanelDistinctId = mixpanelAPI.getDistinctId();
        result.success(mixpanelDistinctId);
    }

    private void initMixPanel(String token, Result result) {
        mixpanelAPI = MixpanelAPI.getInstance(activity, token);
        result.success("Init success..");
    }

    private void identify(String id, Result result) {
        mixpanelAPI.identify(id);
        result.success("Identify success..");
    }

    private void identifyPeople(String id, Result result) {
        mixpanelAPI.identify(id);
        mixpanelAPI.getPeople().identify(id);
        result.success("Identify people success..");
    }

    private void alias(String id, Result result) {
        mixpanelAPI.alias(id, mixpanelAPI.getDistinctId());
        result.success("Alias success..");
    }

    private void setPeopleProperties(MethodCall call, Result result) {
        try {
            if (call.arguments == null) {
                result.error("Parse Error", "Arguments required for setPeopleProperties platform call", null);
            } else {
                JSONObject json = new JSONObject(call.arguments.toString());
                mixpanelAPI.getPeople().set(json);
                result.success("Set People Properties success..");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setOnce(MethodCall call, Result result) {
        try {
            if (call.arguments == null) {
                result.error("Parse Error", "Arguments required for setOnce platform call", null);
            } else {
                JSONObject json = new JSONObject(call.arguments.toString());
                mixpanelAPI.getPeople().setOnce(json);
                result.success("Set Once success..");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void incrementPeopleProperties(MethodCall call, Result result) {
        try {
            if (call.arguments == null) {
                result.error("Parse Error", "Arguments required for incrementPeopleProperties platform call", null);
            } else if (mixpanelAPI.getPeople() != null) {
                Map<String, Double> map = jsonToNumberMap(new JSONObject(call.arguments.toString()));
                mixpanelAPI.getPeople().increment(map);
                result.success("Increment People Properties success..");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void registerSuperProperties(MethodCall call, Result result) {
        try {
            if (call.arguments == null) {
                result.error("Parse Error", "Arguments required for registerSuperProperties platform call", null);
            } else {
                JSONObject json = new JSONObject(call.arguments.toString());
                mixpanelAPI.registerSuperProperties(json);
                result.success("Register Properties success..");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void reset(Result result) {
        mixpanelAPI.reset();
        result.success("Reset success..");
    }

    private void flush(Result result) {
        mixpanelAPI.flush();
        result.success("Reset success..");
    }

    private void showNotification() {
        if (mixpanelAPI.getPeople() != null)
            mixpanelAPI.getPeople().showNotificationIfAvailable(FlutterMixpanelPlugin.activity);
    }

    private void clearPushRegistrationId() {
        if (mixpanelAPI.getPeople() != null)
            mixpanelAPI.getPeople().clearPushRegistrationId();
    }

    private void pushRegistrationId(String id, Result result) {
        if (mixpanelAPI.getPeople() != null)
            mixpanelAPI.getPeople().setPushRegistrationId(id);
    }

    private void trackEvent(MethodCall call, Result result) {
        try {
            if (call.arguments == null) {
                mixpanelAPI.track(call.method);
            } else {
                JSONObject json = new JSONObject(call.arguments.toString());
                mixpanelAPI.track(call.method, json);
            }

            result.success("Track success..");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        if (FlutterMixpanelPlugin.activity == null) {
            FlutterMixpanelPlugin.activity = binding.getActivity();
        }
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {

    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {

    }

    @Override
    public void onDetachedFromActivity() {

    }
}
