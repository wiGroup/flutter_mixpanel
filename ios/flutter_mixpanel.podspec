#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutter_mixpanel.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_mixpanel'
  s.version          = '0.0.1'
  s.summary          = 'A flutter plugin for native Mixpanel SDKs.'
  s.description      = <<-DESC
A flutter plugin for native Mixpanel SDKs.
                       DESC
  s.homepage         = 'http://virginmoneyspot.co.za'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Shaun Norton' => 'shaun@fluidity.solutions' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.dependency 'Mixpanel-swift', '2.7.7'
  s.platform = :ios, '11.0'

  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.swift_version = '5.2'
end
