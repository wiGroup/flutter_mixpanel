#import "FlutterMixpanelPlugin.h"
#if __has_include(<flutter_mixpanel/flutter_mixpanel-Swift.h>)
#import <flutter_mixpanel/flutter_mixpanel-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_mixpanel-Swift.h"
#endif

@implementation FlutterMixpanelPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterMixpanelPlugin registerWithRegistrar:registrar];
}
@end
