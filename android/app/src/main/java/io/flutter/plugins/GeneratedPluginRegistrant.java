package io.flutter.plugins;

import io.flutter.plugin.common.PluginRegistry;
import co.za.wigroup.flutter_mixpanel.FlutterMixpanelPlugin;

/**
 * Generated file. Do not edit.
 */
public final class GeneratedPluginRegistrant {
  public static void registerWith(PluginRegistry registry) {
    if (alreadyRegisteredWith(registry)) {
      return;
    }
    FlutterMixpanelPlugin.registerWith(registry.registrarFor("co.za.wigroup.flutter_mixpanel.FlutterMixpanelPlugin"));
  }

  private static boolean alreadyRegisteredWith(PluginRegistry registry) {
    final String key = GeneratedPluginRegistrant.class.getCanonicalName();
    if (registry.hasPlugin(key)) {
      return true;
    }
    registry.registrarFor(key);
    return false;
  }
}
